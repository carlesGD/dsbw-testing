package unit

import org.scalatest.{BeforeAndAfter, FlatSpec}
import org.mockito.Mockito
import museums.{MuseumObject, MuseumApi, Repository}
import server.HttpStatusCode

class MuseumApiSpec extends FlatSpec with BeforeAndAfter {

  var repository: Repository = null
  var api: MuseumApi = null

  before {
    repository = Mockito.mock(classOf[Repository])
    api = new MuseumApi(repository)
  }

  "The Museum API" should "get an object" in {
    val id:Long = 5
    val obj = Some(MuseumObject(id, "Rembrandt", "My painting"))
    Mockito.when(repository.find(id)).thenReturn(obj)

    assert(api.getObject(s"/$id").body === obj)
    assert(api.getObject(s"/$id").status === HttpStatusCode.Ok)

  }
}
